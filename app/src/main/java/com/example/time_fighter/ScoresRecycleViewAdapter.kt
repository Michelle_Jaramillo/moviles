package com.example.time_fighter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ScoresRecycleViewAdapter:RecyclerView.Adapter<ScoreViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScoreViewHolder {
        val view =LayoutInflater.from(parent.context).inflate(R.layout.scores_view_holder,parent,false)
        return ScoreViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(holder: ScoreViewHolder, position: Int) {
        holder.playerNamer.text="Michelle"
        holder.playerScore.text=100.toString()
    }

}