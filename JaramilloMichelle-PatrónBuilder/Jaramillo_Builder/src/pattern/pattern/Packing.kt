package pattern

interface Packing {
    fun pack(): String
}