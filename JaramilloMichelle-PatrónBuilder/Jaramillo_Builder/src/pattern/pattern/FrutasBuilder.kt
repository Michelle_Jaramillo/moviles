package pattern

class FrutasBuilder {
    fun prepareConCascara(): Alimentos {
        val alimento = Alimentos()
        alimento.addItem(FrutasSinCascara())
        return alimento
    }

    fun prepareSinCascara(): Alimentos {
        val alimento = Alimentos()
        alimento.addItem(FrutasConCascara())
        return alimento
    }
}