package pattern

class FrutasConCascara : Frutas() {
    override fun name(): String = "Frutas con cascara comestible"

    override fun price(): Float = 1.5f
}