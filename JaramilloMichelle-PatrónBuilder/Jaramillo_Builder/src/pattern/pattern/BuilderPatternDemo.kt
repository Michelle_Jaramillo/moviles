package pattern

fun main(args: Array<String>) {
    val frutasBuilder = FrutasBuilder()
    val comestible = frutasBuilder.prepareConCascara()
    println("Frutas con cascara comestible")
    comestible.showItems()
    println("Total Costo: ${comestible.getCost()}")

    val sincascara = frutasBuilder.prepareSinCascara()
    println("\n\nFrutas sin cascara comestible")
    sincascara.showItems()
    println("Total Costo: ${sincascara.getCost()}")
}