package pattern

class FrutasSinCascara : Frutas() {
    override fun name(): String = "Frutas con cascara no comestible"

    override fun price(): Float = 2.3f
}