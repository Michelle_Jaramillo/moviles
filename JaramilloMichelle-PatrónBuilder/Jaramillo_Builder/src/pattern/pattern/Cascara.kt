package pattern

class Cascara : Packing {
    override fun pack(): String {
        return "Cascara"
    }
}