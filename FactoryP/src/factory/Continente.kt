package factory

enum class Continente
{
    EstadosUnidos, Espania, UK, Grecia


}

fun dinero(continente: Continente):IDinero?{
    when (continente) {
       Continente.Espania, Continente.Grecia-> return Euro()
        Continente.EstadosUnidos -> return EstadosUnidosDolar()
       else -> return null

    }
}

