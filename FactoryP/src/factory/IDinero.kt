package factory

interface IDinero{

    fun simbolo(): String
    fun codigo(): String
}