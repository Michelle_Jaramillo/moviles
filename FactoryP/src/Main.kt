import factory.Continente
import factory.dinero


fun main(args: Array<String>) {
    val noCurrencyCode = "\"ISO Es una Moneda no Definida\""

    println(dinero(Continente.Grecia)?.codigo() ?: noCurrencyCode)
    println(dinero(Continente.Espania)?.codigo() ?: noCurrencyCode)
    println(dinero(Continente.EstadosUnidos)?.codigo() ?: noCurrencyCode)
    println(dinero(Continente.UK)?.codigo() ?: noCurrencyCode)
}